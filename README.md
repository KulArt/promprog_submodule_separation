# submodule separation

Here is a sequence of commands to make directory of project as submodule:
```
git clone -b r1.14 --single-branch https://github.com/tensorflow/tensorflow.git // clone tensorflow branch
cd tensorflow
git filter-branch --subdirectory-filter tensorflow/tensorflow/contrib -- --all // filter directory only from branch
git remote rm origin
git remote add origin submodule-repo-link
// push all to submodule repo
cd origin-project-dir
git submodule add submodule-repo-link // add submodule
```